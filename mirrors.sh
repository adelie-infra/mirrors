#!/bin/sh -e

DESTFILE=/var/www/status.html;

#---------------------------------------------------------------

##
# Generates table data for https://www.adelielinux.org/mirrors/.
# The output is HTML and it is run on a 5-minute cron job, then
# the result is made available to clients accessing that page.
#

##
# Table header.
#
T0_NEED=1;
T0_BODY=$(cat <<'EOF'
<thead>
    <tr>
	    <th scope="col"><!-- country flag --></th>
	    <th scope="col">Location</th>
	    <th scope="col">Name</th>
	    <th scope="col" class="text-right">Speed</th>
	    <th scope="col" class="text-right">Freshness</th>
	    <th scope="col" class="text-right">Uptime</th>
	    <th scope="col" class="text-right">Sponsor</th>
	    <th scope="col">Protocol</th>
    </tr>
</thead>
EOF
);


#---------------------------------------------------------------

##
# Tier 1.
#
T1_NEED=1;
T1_BODY=$(cat <<'EOF'
    <tr>
        <td colspan="8"><strong>Official</strong>&nbsp;&mdash;&nbsp;Part of Adélie's core infrastructure.</td>
    </tr>
EOF
);

##
# Tier 2.
#
T2_NEED=1;
T2_BODY=$(cat <<'EOF'
    <tr>
        <td colspan="8"><strong>Affiliated</strong>&nbsp;&mdash;&nbsp;Operated by Adélie's developers and friends.</td>
    </tr>
EOF
);

##
# Tier 3.
#
T3_NEED=1;
T3_BODY=$(cat <<'EOF'
    <tr>
        <td colspan="8"><strong>Third-Party</strong>&nbsp;&mdash;&nbsp;Community-managed mirrors over which we have no control.</td>
    </tr>
EOF
);


#---------------------------------------------------------------

##
# Row template.
#
# It is assumed that all mirrors have at least 'http' meaning if
# one does not, then there is an extra preceding '<br />' which
# may cause display problems.
#
ROW_DATA=$(cat <<'EOF'
    <tr>
        <td class="text-right"><img src="../assets/images/flags/w40-webp/__FLAG__.webp" alt=""></td>
        <td>__STATE__,<br />__COUNTRY__</td>
        <td>__NAME__</td>
        <td class="text-right">__SPEED__</td>
        <td class="text-right">__FRESH__</td>
        <td class="text-right">__UPTIME__</td>
        <td class="text-right">__SPONSOR__</td>
        <td>
            <strong>
                      <a href="__HTTP__">http</a>
                <br /><a href="__HTTPS__">https</a>
                <br /><a href="__RSYNC__">rsync</a>
                <br /><a href="__FTP__">ftp</a>
            </strong>
        </td>
    </tr>
EOF
);


#---------------------------------------------------------------

##
# Convert integer seconds into "0d 00:00:00" format.
#
sec2str ()
{
    T=$1;
    D=$((T/60/60/24));
    H=$((T/60/60%24));
    M=$((T/60%60));
    S=$((T%60));

    printf '%dd %02d:%02d:%02d' $D $H $M $S;
}


#---------------------------------------------------------------

##
# We define freshness as one of three scenarios. (1) Under 24h
# is "current" due to heartbeat being daily cron. (2) Any value
# converted to the human-friendly string. (3) No heartbeat found
# is "never", which occurs in the case of mirrors not set up.
#
# The 'heartbeat' file is downloaded once from mirrormaster each
# time this script is run, so we do not do that in this loop.
#
do_fresh ()
{
    # some mirrors may be partial
    if ! curl -fs -m 2 -o ${1}.beat ${2}/adelie/heartbeat; then
         curl -fs -m 2 -o ${1}.beat ${2}/heartbeat;
        where=${2}/heartbeat;
    else
        where=${2}/adelie/heartbeat;
    fi

    # 200 or die
    case $(curl -fs -m 2 -o /dev/null -I -w "%{http_code}" ${where}) in
        200) : ;;
        *) rm ${1}.beat; ;;
    esac

    status=;
    timer=;

    # we actually have 200
    if test -e ${1}.beat; then
        # file is empty (unknown condition)
        if ! test -s ${1}.beat; then
            status=dark;
            timer=unknown;
        else
            # either we are in the 24-hour enveloper or not
            delta=$(($(cat heartbeat) - $(cat ${1}.beat)));
            if test ${delta} -lt $((24*60*60)); then
                status=primary;
                timer=current;
            else
                status=warning;
                timer=$(sec2str ${delta});
            fi
        fi
    else
        status=danger;
        timer=never;
    fi

    printf '<span class="badge badge-pill badge-%s">%s</span>' "${status}" "${timer}";
}


#---------------------------------------------------------------

##
# We define uptime and downtime as "time since last pass/fail"
# via HTTP.
#
do_uptime ()
{
    # record last success or failure
    if curl -m 2 -i -o /dev/null ${2}; then
        test -e ${1}.pass || touch ${1}.pass;
        rm -f ${1}.fail;
    else
        test -e ${1}.fail || touch ${1}.fail;
        rm -f ${1}.pass;
    fi

    status=;
    testme=;

    if test -e ${1}.fail; then
        status=danger;
        testme=${1}.fail;
    elif test -e ${1}.pass; then
        status=success;
        testme=${1}.pass;
    else
        exit 1; # unexpected state
    fi

    delta=$(($(date +%s) - $(date +%s -r ${testme})))
    timer=$(sec2str ${delta});

    printf '<span class="badge badge-pill badge-%s">%s</span>' "${status}" "${timer}";
}


#---------------------------------------------------------------

##
# Get time of last mirrormaster update.
#
wget -q http://mirrormaster.adelielinux.org/adelie/heartbeat -O heartbeat;

##
# Populate templates. The logic here is as follows:
#
#   * Read space-delimited CSV file with a set number of columns
#
#   * Each column corresponds to a variable accessible by 'read'
#
#   * Append a row template to the output file
#
#   * Values are replaced directly, or computed, or deleted if a
#     value is '_', the sentinel for NULL because we cannot have
#     meaningful extra spaces when reading by column.
#
#   * Output is written to a temporary file
#
# Note that input data must be space-escaped.
#
TEMPFILE=$(mktemp);
printf >> ${TEMPFILE} "${T0_BODY}\n";
printf >> ${TEMPFILE} "<tbody>\n";
while read type flag state country name speed sponsor http https rsync ftp; do

    case $type in
        T1) test ${T1_NEED} = 1 && T1_NEED=0 && printf >> ${TEMPFILE} "${T1_BODY}\n"; ;;
        T2) test ${T2_NEED} = 1 && T2_NEED=0 && printf >> ${TEMPFILE} "${T2_BODY}\n"; ;;
        T3) test ${T3_NEED} = 1 && T3_NEED=0 && printf >> ${TEMPFILE} "${T3_BODY}\n"; ;;
        *) printf "E: bad type\n"; exit 1; ;;
    esac

    printf "%s\n" "${ROW_DATA}" >> ${TEMPFILE};

    sed -i ${TEMPFILE}                                         \
        -e 's@__FLAG__@'"${flag}"'@g'                          \
        -e 's@__STATE__@'"${state}"'@g'                        \
        -e 's@__COUNTRY__@'"${country}"'@g'                    \
        -e 's@__NAME__@'"${name}"'@g'                          \
        -e 's@__SPEED__@'"${speed}"'@g'                        \
        -e 's@__SPONSOR__@'"${sponsor}"'@g'                    \
        -e 's@__FRESH__@'"$(do_fresh ${name} ${http})"'@g'     \
        -e 's@__UPTIME__@'"$(do_uptime ${name} ${http})"'@g'   \
        ;

    if test ${http} != _; then
        sed -i ${TEMPFILE}                                     \
            -e 's@__HTTP__@'"${http}"'@g'                      \
            ;
    else
        sed -i ${TEMPFILE}                                     \
            -e '/__HTTP__/d'                                   \
            ;
    fi

    if test ${https} != _; then
        sed -i ${TEMPFILE}                                     \
            -e 's@__HTTPS__@'"${https}"'@g'                    \
            ;
    else
        sed -i ${TEMPFILE}                                     \
            -e '/__HTTPS__/d'                                  \
            ;
    fi

    if test ${rsync} != _; then
        sed -i ${TEMPFILE}                                     \
            -e 's@__RSYNC__@'"${rsync}"'@g'                    \
            ;
    else
        sed -i ${TEMPFILE}                                     \
            -e '/__RSYNC__/d'                                  \
            ;
    fi

    if test ${ftp} != _; then
        sed -i ${TEMPFILE}                                     \
            -e 's@__FTP__@'"${ftp}"'@g'                        \
            ;
    else
        sed -i ${TEMPFILE}                                     \
            -e '/__FTP__/d'                                    \
            ;
    fi

done < mirrors.csv
printf >> ${TEMPFILE} "</tbody>\n";


#---------------------------------------------------------------

mv ${TEMPFILE} ${DESTFILE};
chmod 644 ${DESTFILE};
